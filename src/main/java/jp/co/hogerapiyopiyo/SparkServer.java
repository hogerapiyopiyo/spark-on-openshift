package jp.co.hogerapiyopiyo;

import static spark.Spark.*;
import java.util.Optional;

public class SparkServer {

    public static void main(String[] args) {

        // IP設定
        ipAddress(Optional.ofNullable(System.getenv("IP_ADDRESS")).orElse("0.0.0.0"));

        // ポート設定
        // 2.1だとsetPortはDeprecatedになってた
        // http://spark.screenisland.com/spark/SparkBase.html#setPort-int-
        port(Integer.parseInt(Optional.ofNullable(System.getenv("PORT")).orElse("5353")));

        // ルーティング設定
        get("/hello", (req, res) -> "Hello World");
    }
}
